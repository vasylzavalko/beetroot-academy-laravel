<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OpenWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:openweather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $city = [
            1 => ["Київ", 703448],
            2 => ["Львів", 702550],
            3 => ["Івано-Франківськ", 707471],
        ];
        $cityKey = array_keys($city);
        
        $ask = "Оберіть місто: ";
        foreach($city as $key => $value){
            $ask .= $value[0]." - ".$key.", ";
        }
        $id = $this->ask($ask);
        
        if( in_array($id, $cityKey) ) {
            
            $url = "https://api.openweathermap.org/data/2.5/weather?id=".$city[$id][1]."&appid=06347872fb7d0895381eb6fd72d91863&units=metric&lang=ua";
            $json = file_get_contents($url);
            $data = json_decode($json, true);
  
            $temperature = $data["main"]["temp"];
            $sunrise = $data["sys"]["sunrise"]+$data["timezone"];
            $sunset = $data["sys"]["sunset"]+$data["timezone"];
            $wind = $data["wind"]["speed"];
            
            $this->info("Погода в місті ".$city[$id][0]);
            $this->info("Температура повітря (в градусах) ".$temperature);
            $this->info("Швидкість вітру ".$wind." м/с");
            $this->info("Сонце зійде о ".date("H:i:s", $sunrise));
            $this->info("Сонце зайде о ".date("H:i:s", $sunset));
            
        } else {
            $this->error('Місто не знайдено!');
        }
        
        return 0;
    }
}

