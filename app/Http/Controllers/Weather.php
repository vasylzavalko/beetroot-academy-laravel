<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dnsimmons\OpenWeather\OpenWeather;

class Weather extends Controller
{
    
    public function __construct()
    {
        
        $this->city = [
            1 => ["Київ", 703448],
            2 => ["Львів", 702550],
            3 => ["Івано-Франківськ", 707471],
            4 => ["Житомир", 686967],
            5 => ["Тернопіль", 691650],
            6 => ["Дніпро", 709930],
            7 => ["Одеса", 698740],
        ];
        
    }    
    
    public function index()
    {
        
        $cityStart = 1;
        
        $weather = new OpenWeather();
        $current = $weather->getCurrentWeatherByCityId($this->city[$cityStart][1], "metric");
        
        return view('weather/index', [
            'weather' => $current,
            'city' => $this->city,
            'cityActive' => $this->city[$cityStart],
        ]);
        
    }
    
    public function city(Request $request)
    {
        
        if ($request->method()!="POST") {
            return redirect('/weather');
        }
        
        $cityId = $request->input("city");
        
        if (!is_numeric($cityId)) {
            return redirect('/weather');
        }
        
        $weather = new OpenWeather();
        $current = $weather->getCurrentWeatherByCityId($this->city[$cityId][1], "metric");
        
        return view('weather/index', [
            'weather' => $current,
            'city' => $this->city,
            'cityActive' => $this->city[$cityId],
        ]);
        
    }
    
}
