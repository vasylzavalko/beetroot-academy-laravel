
<h1>Погода м. <?php echo $cityActive[0] ?></h1>
<div style="display:flex;">
<p><?php echo $weather["forecast"]["temp"]; ?> <sup>o</sup>C</p>
<img src="<?php echo $weather["condition"]["icon"]; ?>">
</div>
<hr>
<h2>Обрати інше місто</h2>
<form method="POST" action="/weather/city">
    @csrf
    <select name="city">
        <?php foreach($city as $key => $value) { ?>
        <option value="<?php echo $key; ?>"><?php echo $value[0]; ?></option>
        <?php } ?>
    </select>
    <button type="submit">Надіслати</button>
</form>